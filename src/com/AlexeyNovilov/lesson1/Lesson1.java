package com.AlexeyNovilov.lesson1;

public class Lesson1 {
    int z;
    int o;
    String randomString = "abcdef";

    public static void main(String[] args) {
        new Lesson1().go(5, 7);

    }

    public void go(int x, int y) {
        z = x + y;
        for (int i = 0; i < 10; i++) {
            z++;
            System.out.println(z);
        }
        o = z - x;
        System.out.println(o);
    }
}